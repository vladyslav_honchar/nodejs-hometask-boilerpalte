const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    getOneFighter(id) {
        const fighter = FighterRepository.getOne((fighter) => fighter.id === id);
        if (fighter) {
            return fighter;
        }

        const err = {
        error: true,
        message: 'Fighter not found',
        }
        
        throw err;
    }

    getAllFighters() {
        return FighterRepository.getAll();
    }

    addFighter(fighter) {
        if (FighterRepository.getOne((entity) => entity.name.toLowerCase() === fighter.name.toLowerCase())) {
            const err = {
                error: true,
                message: 'Fighter with this name already exists',
            }
                
            throw err;
        }

        return FighterRepository.create(fighter);
    }

    updateFighter(id, dataToUpdate) {
        if (
            FighterRepository.getOne((dbFighter) => {
                return ((dataToUpdate.name !== undefined) &&
                (dbFighter.name.toUpperCase() === dataToUpdate.name.toUpperCase()) &&
                (dbFighter.id !== id));

            })
        ) {
            const err = {
                error: true,
                message: 'Fighter with this name already exists',
            };
                
            throw err;
        } else if (this.getOneFighter(id)) {
          return FighterRepository.update(id, dataToUpdate);
        }
    }

    deleteFighter(id) {
        if (this.getOneFighter(id)) {
          FighterRepository.delete(id);
        } 
    }
}

module.exports = new FighterService();