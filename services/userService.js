const { UserRepository } = require('../repositories/userRepository');

class UserService {
    getOneUser(id) {
        const user = UserRepository.getOne((user) => user.id === id);
        if (user) {
            return user;
        }

        const err = {
        error: true,
        message: 'User not found',
        }
        
        throw err;
    }

    getAllUsers() {
        return UserRepository.getAll();
    }

    addUser(user) {
        if (UserRepository.getOne((dbUser) => dbUser.email.toUpperCase() === user.email.toUpperCase())) {
            const err = {
                error: true,
                message: 'User with this email already exists',
                }
                
                throw err;
        } else if (UserRepository.getOne((dbUser) => dbUser.phoneNumber === user.phoneNumber)) {
            const err = {
                error: true,
                message: 'User with this phonenumber already exists',
                }
                
                throw err;
        }
        return UserRepository.create(user);
    }

    updateUser(id, dataToUpdate) {
        if (
            UserRepository.getOne((dbUser) => {
                return ((dataToUpdate.email !== undefined) &&
                (dbUser.email.toUpperCase() === dataToUpdate.email.toUpperCase()) &&
                (dbUser.id !== id));

            })
        ) {
            const err = {
                error: true,
                message: 'User with this email already exists',
            };
                
            throw err;
        } else if (
            UserRepository.getOne((dbUser) => (dbUser.phoneNumber === dataToUpdate.phoneNumber) && (dbUser.id !== id))
        ) {
            const err = {
                error: true,
                message: 'User with this phonenumber already exists',
            };
                
            throw err;
        } else if (this.getOneUser(id)) {
          return UserRepository.update(id, dataToUpdate);
        }
    }

    deleteUser(id) {
        if (this.getOneUser(id)) {
          UserRepository.delete(id);
        } 
    }
}

module.exports = new UserService();