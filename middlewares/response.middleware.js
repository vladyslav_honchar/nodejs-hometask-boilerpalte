const responseMiddleware = (req, res, next) => {
    res.status(200).send(res.data);
}

exports.responseMiddleware = responseMiddleware;