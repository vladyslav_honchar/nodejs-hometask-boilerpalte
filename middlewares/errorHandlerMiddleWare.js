const errorHandlerMiddleWare = (err, req, res, next) => {
    console.log("ERROR HANDLER");
    if (
      err.message === 'User entity to update from isn\'t valid' ||
      err.message === 'User entity to create isn\'t valid' || 
      err.message === 'User with this phonenumber already exists' ||
      err.message === 'User with this email already exists' ||
      err.message === 'Fighter with this name already exists'
    ) {
      res.status(400).send(err);
    } else if(err.message === 'User not found' || err.message === 'Fighter not found') {
      res.status(404).send(err);
    } else {
      res.status(400).send(err);
    }
    
};

exports.errorHandlerMiddleWare = errorHandlerMiddleWare;

