const { user } = require('../models/user');

const userValidator = {
    firstName: (value) => (value !== undefined) && (value.length > 0),
    lastName: (value) => (value !== undefined) && (value.length > 0),
    email: (value) => (value !== undefined) && /^[a-zA-Z0-9]+[a-zA-Z0-9.]*@gmail\.com$/g.test(value),
    phoneNumber: (value) => (value !== undefined) && /^\+380\d{9}$/g.test(value),
    password: (value) => (value !== undefined) && (value.length >= 3),
};

const isValidKeyOfRequestUser = (requestUser) =>  {
    for (const reqUserKey of Object.keys(requestUser)) {
        if (!Object.keys(userValidator).includes(reqUserKey)) {
            return false;
        }
    }

    return true;
};

const hasAllkeys = (requestUser) =>  {
    for (const userKey of Object.keys(userValidator)) {
        if (!Object.keys(requestUser).includes(userKey)) {
            return false;
        }
    }

    return true;
};

const hasValidValues = (requestUser) =>  {
    for (const reqUserKey of Object.keys(requestUser)) {
        console.log(reqUserKey);
        if (!userValidator[reqUserKey](requestUser[reqUserKey])) {
            return false;
        }
    }

    return true;
};

const createUserValid = (req, res, next) => {
    const requestUser = req.body;
    if (
        isValidKeyOfRequestUser(requestUser) &&
        hasAllkeys(requestUser) &&
        hasValidValues(requestUser)
    ) {
        next();
    } else {
        const err = {
            error: true,
            message: 'User entity to create isn\'t valid',
        }
        throw err;
    }
}

const updateUserValid = (req, res, next) => {
    const requestUser = req.body;
    if (
        isValidKeyOfRequestUser(requestUser) &&
        Object.keys(requestUser).length > 0 &&
        hasValidValues(requestUser)
    ) {
        next();
    } else {
        const err = {
            error: true,
            message: 'User entity to update from isn\'t valid',
        }
        throw err;
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;