const { fighter } = require('../models/fighter');

const fighterValidator = {
    name: (value) => (value !== undefined) && (value.length > 0),
    health: (value) => (value !== undefined) && (typeof value === 'number') && (value > 80) && (value < 120),
    power: (value) => (value !== undefined) && (typeof value === 'number') && (value < 100) && (value > 1),
    defense: (value) => (value !== undefined) && (typeof value === 'number') && (value < 10) && (value > 1),
};

const isValidKeyOfRequestFighter = (requestFighter) =>  {
    for (const reqFighterKey of Object.keys(requestFighter)) {
        if (!Object.keys(fighterValidator).includes(reqFighterKey)) {
            return false;
        }
    }

    return true;
};

const hasAllkeys = (requestFighter) =>  {
    for (const fighterKey of Object.keys(fighterValidator)) {
        if (!Object.keys(requestFighter).includes(fighterKey)) {
            return false;
        }
    }

    return true;
};

const hasValidValues = (requestFighter) =>  {
    for (const reqFighterKey of Object.keys(requestFighter)) {
        console.log(reqFighterKey);
        if (!fighterValidator[reqFighterKey](requestFighter[reqFighterKey])) {
            return false;
        }
    }
    return true;
};

const createFighterValid = (req, res, next) => {
    const requestFighter = req.body;
    if(requestFighter.health === undefined) requestFighter.health = 100;
    if (
        isValidKeyOfRequestFighter(requestFighter) &&
        hasAllkeys(requestFighter) &&
        hasValidValues(requestFighter)
    ) {
        next();
    } else {
        const err = {
            error: true,
            message: 'Fighter entity to create isn\'t valid',
        }
        throw err;
    }
}

const updateFighterValid = (req, res, next) => {
    const requestFighter = req.body;
    if (
        isValidKeyOfRequestFighter(requestFighter) &&
        Object.keys(requestFighter).length > 0 &&
        hasValidValues(requestFighter)
    ) {
        next();
    } else {
        const err = {
            error: true,
            message: 'Fighter entity to update from isn\'t valid',
        }
        throw err;
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;