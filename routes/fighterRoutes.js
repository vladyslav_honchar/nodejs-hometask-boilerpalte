const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const { errorHandlerMiddleWare } = require('../middlewares/errorHandlerMiddleWare');


const router = Router();

router.get(
    '/',
    (req, res, next) => {
      const fighters = FighterService.getAllFighters();
      res.data = fighters;
      next();
    },
    responseMiddleware
  ).get(
    '/:id',
    (req, res, next) => {
      const id = req.params.id;
      const fighter = FighterService.getOneFighter(id);
      res.data = fighter;
      next();
    },
    responseMiddleware
);
  
router.post(
    '/',
    createFighterValid,
    (req, res, next) => {
      const newFighter = FighterService.addFighter(req.body);
      res.data = newFighter;
      next();
    },
    responseMiddleware
  )
  
  router.put(
    '/:id',
    updateFighterValid,
    (req, res, next) => {
      const id = req.params.id;
      const dataToUpdate = req.body;
      const updatedFighter = FighterService.updateFighter(id, dataToUpdate);
      res.data = updatedFighter;
      next();
    },
    responseMiddleware
  )
  
  router.delete(
    '/:id',
    (req, res, next) => {
      const id = req.params.id;
      const deletedFighter = FighterService.getOneFighter(id);
      FighterService.deleteFighter(id);
      res.data = deletedFighter;
      next();
    },
    responseMiddleware
  )

  router.use(errorHandlerMiddleWare, responseMiddleware);

module.exports = router;