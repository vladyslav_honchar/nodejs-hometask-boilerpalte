const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { errorHandlerMiddleWare } = require('../middlewares/errorHandlerMiddleWare');

const router = Router();

router.get(
    '/',
    (req, res, next) => {
      const users = UserService.getAllUsers();
      res.data = users;
      next();
    },
    responseMiddleware
  ).get(
    '/:id',
    (req, res, next) => {
      const id = req.params.id;
      const user = UserService.getOneUser(id);
      res.data = user;
      next();
    },
    responseMiddleware
);
  
router.post(
    '/',
    createUserValid,
    (req, res, next) => {
      const newUser = UserService.addUser(req.body);
      res.data = newUser;
      next();
    },
    responseMiddleware
  )
  
  router.put(
    '/:id',
    updateUserValid,
    (req, res, next) => {
      const id = req.params.id;
      const dataToUpdate = req.body;
      const updatedUser = UserService.updateUser(id, dataToUpdate);
      res.data = updatedUser;
      next();
    },
    responseMiddleware
  )
  
  router.delete(
    '/:id',
    (req, res, next) => {
      const id = req.params.id;
      const deletedUser = UserService.getOneUser(id);
      UserService.deleteUser(id);
      res.data = deletedUser;
      next();
    },
    responseMiddleware
  )

  router.use(errorHandlerMiddleWare, responseMiddleware);

module.exports = router;